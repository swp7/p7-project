﻿using System;
using System.Collections.Generic;
using Bunit;
using Bunit.TestDoubles.Authorization;
using Bunit.TestDoubles.JSInterop;
using Common.Data;
using FrontEnd.Pages.CoursePages;
using FrontEnd.Pages.CoursePages.Components;
using FrontEnd.PubSub;
using FrontEnd.Services;
using FrontEnd.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Telerik.JustMock;
using TestContext = Bunit.TestContext;

namespace FrontEnd.Tests.CoursePages.Components {
	public class HelpComponentTest {
		//Test Context
		private TestContext _ctx;
		
		//Mock Services
		private IQuestionService _questionServiceMock;
		
		//Mocks of ViewModels
		private TinyCourseViewModel _tinyCourseViewModelMock;
		private PhysicalQuestionViewModel _physicalQuestionViewModelMock;
		
		//Used for rendering the component
		private IRenderedComponent<HelpComponent> _cut;

		//Mock PhysicalQuestions
		private List<PhysicalQuestion> _mockPhysicalQuestions;

		[SetUp]
		public void Setup() {
			_ctx = new TestContext();
			_ctx.Services.AddTestAuthorization();
			_ctx.Services.AddRazorPages();
			_ctx.Services.AddMockJSRuntime();

			//Create Mock services
			_ctx.Services.AddSingleton<PhysicalQuestionSubscription>();
			
			_questionServiceMock = Mock.Create<IQuestionService>();
			_ctx.Services.AddSingleton(_questionServiceMock);

			//Create Mock ViewModels
			_tinyCourseViewModelMock = Mock.Create<TinyCourseViewModel>();
			_ctx.Services.AddSingleton(_tinyCourseViewModelMock);

			_physicalQuestionViewModelMock = Mock.Create<PhysicalQuestionViewModel>();
			_ctx.Services.AddSingleton(_physicalQuestionViewModelMock);

			//Create Mock PhysicalQuestions
			_mockPhysicalQuestions = new List<PhysicalQuestion> {new PhysicalQuestion(), new PhysicalQuestion()};

			//Arrange the mock methods required to render the component
			Mock.Arrange(
				() => _tinyCourseViewModelMock.Course
			).Returns(new TinyCourse("Name of course", 2077, false, new IdentityUser(), "not a good subtitle"));

			Mock.Arrange(
				() => _physicalQuestionViewModelMock.Questions
			).Returns(_mockPhysicalQuestions);
			
			Mock.Arrange(
				() => _tinyCourseViewModelMock.SelectedSession
			).Returns(new Session("testSession", DateTime.Now, DateTime.Now));

			//Render the component
			_cut = _ctx.RenderComponent<HelpComponent>(("PhysicalQuestionViewModel", _physicalQuestionViewModelMock),ComponentParameterFactory.CascadingValue("MYCODE"), 
				ComponentParameterFactory.CascadingValue(_tinyCourseViewModelMock), ComponentParameterFactory.CascadingValue(new SessionComponent()),
				ComponentParameterFactory.CascadingValue(new IdentityUser()));
		}

		[Test]
		public void AskQuestionButtonDisabledWhenAllSessions() {
			//Arrange
			Mock.Arrange(
				() => _tinyCourseViewModelMock.SelectedSession
			).Returns(new Session("ALLSESSIONS", DateTime.Now, DateTime.Now));

			//Re-Render component to update elements relying on selected session 
			_cut.Render();
			
			
			//Act

			//Assert
			Assert.True(_cut.Instance.ButtonIsDisabled);
		}
		
		[Test]
		public void AskQuestionButtonEnabledWhenSessionSelected() {
			//Arrange
			
			
			//Act
			
			
			//Assert
			Assert.False(_cut.Instance.ButtonIsDisabled);
		}
		
		[Test]
		public void AskQuestionButtonOpensDialog() {
			//Arrange
			
			
			//Act
			_cut.Find("#AskQuestionButton").Click();

			//Assert
			Assert.True(_cut.Instance.QuestionDialog.IsDialogOpen);
		}
		
		[Test]
		public void DisplayCorrectNumberOfHelpItems() {
			//Arrange
			
			
			//Act
			
			
			//Assert
			Assert.AreEqual(_mockPhysicalQuestions.Count, _cut.FindComponents<HelpQueueItem>().Count);
		}
	}
}