﻿using System;
using System.Collections.Generic;
using Bunit;
using Bunit.TestDoubles.Authorization;
using Bunit.TestDoubles.JSInterop;
using Common.Data;
using FrontEnd.Pages.CoursePages.Components;
using FrontEnd.Services;
using FrontEnd.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Telerik.JustMock;
using TestContext = Bunit.TestContext;

namespace FrontEnd.Tests.CoursePages.Components {
	public class SessionComponentTest {
		//Test Context
		private TestContext _ctx;

		//The services required by the page
		private IRoleService _roleServiceMock;
		private ISessionService _sessionServiceMock;

		//Mocks of ViewModels
		private TinyCourseViewModel _viewModelMock;

		//Used for rendering the component
		private IRenderedComponent<SessionComponent> _cut;
		

		//Mock Sessions
		private Session _mockSession1;
		private Session _mockSession2;
		
		//List of Mock Sessions
		private List<Session> _mockSessions;

		[SetUp]
		public void Setup() {
			_ctx = new TestContext();
			_ctx.Services.AddTestAuthorization();
			_ctx.Services.AddRazorPages();
			_ctx.Services.AddMockJSRuntime();

			//Create Mock services
			_roleServiceMock = Mock.Create<IRoleService>();
			_ctx.Services.AddSingleton(_roleServiceMock);

			_sessionServiceMock = Mock.Create<ISessionService>();
			_ctx.Services.AddSingleton(_sessionServiceMock);

			//Create Mock ViewModels
			_viewModelMock = Mock.Create<TinyCourseViewModel>();
			_ctx.Services.AddSingleton(_viewModelMock);

			//Create Mock Session
			_mockSession1 = new Session("Session1", DateTime.Now, DateTime.Now);
			_mockSession2 = new Session("Session2", DateTime.Now, DateTime.Now);
			
			//Create Mock Sessions
			_mockSessions = new List<Session> {_mockSession1, _mockSession2};

			//Arrange the mock methods required to render the component
			Mock.Arrange(
				() => _viewModelMock.Course
			).Returns(new TinyCourse("Name of course", 2077, false, new IdentityUser(), "not a good subtitle"));

			Mock.Arrange(
				() => _viewModelMock.Sessions
			).Returns(_mockSessions);
			
			Mock.Arrange(
				() => _roleServiceMock.RequestIsUserInRole(Arg.IsAny<IdentityUser>(), Arg.AnyString)
			).Returns(false);

			//Render the component
			_cut = _ctx.RenderComponent<SessionComponent>(("ViewModel",_viewModelMock), ("CourseCode", "MYCODE"), 
				("User", new IdentityUser()));
		}

		[Test]
		public void SessionsHaveCorrectName() {
			//Arrange
			var testSessions = _cut.FindAll("#SessionEntries");
			
			//loop variable
			var i = 0;
			
			//Act
			
			
			//Assert
			Assert.AreEqual(_mockSessions.Count, testSessions.Count);
			while (i < testSessions.Count) {
				testSessions[i].TextContent.MarkupMatches(_mockSessions[i].Name);
				i++;
			}
		}
		
		[Test]
		public void DisplayCorrectNumberOfSessions() {
			//Arrange
			
			
			//Act
			
			
			//Assert
			Assert.AreEqual(_mockSessions.Count, _cut.FindAll("#SessionEntries").Count);
		}

		[Test]
		public void CreateSessionButtonMissingWhenUserNotLecturer() {
			//Arrange
			

			//Act

			
			//Assert
			Assert.AreEqual(0, _cut.FindAll("#CreateSessionButton").Count);
		}
		
		[Test]
		public void CreateSessionButtonIsDisplayedWhenUserIsLecturer() {
			//Arrange
			Mock.Arrange(
				() => _roleServiceMock.RequestIsUserInRole(Arg.IsAny<IdentityUser>(), Arg.AnyString)
			).Returns(true);
			
			//Re-render component to allow Create session button to appear.
			_cut.Render();

			//Act
			

			//Assert
			Assert.AreEqual(1, _cut.FindAll("#CreateSessionButton").Count);
		}
		
		[Test]
		public void CreateSessionButtonOpensDialog() {
			//Arrange
			Mock.Arrange(
				() => _roleServiceMock.RequestIsUserInRole(Arg.IsAny<IdentityUser>(), Arg.AnyString)
			).Returns(true);
			
			//Re-render component to allow Create session button to appear.
			_cut.Render();

			//Act
			_cut.Find("#CreateSessionButton").Click();
			
			//Assert
			Assert.True(_cut.Instance.CreateSessionDialog.DialogIsOpen);
		}
	}
}