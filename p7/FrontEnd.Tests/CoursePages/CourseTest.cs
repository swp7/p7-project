﻿using System;
using System.Collections.Generic;
using Bunit;
using Bunit.TestDoubles.Authorization;
using Bunit.TestDoubles.JSInterop;
using Common.Data;
using FrontEnd.Pages.CoursePages.Components;
using FrontEnd.Pages.QuestionPages;
using FrontEnd.Tests.MockClasses;
using FrontEnd.ViewModels;
using MatBlazor;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Telerik.JustMock;
using Course = FrontEnd.Pages.CoursePages.Course;
using TestContext = Bunit.TestContext;

namespace FrontEnd.Tests.CoursePages {
	public class CourseTest {
		//Test Context
		private TestContext _ctx;

		//Mocks of ViewModels
		private TinyCourseViewModel _viewModelMock;
		
		//Mock Sessions
		private Session _mockSession;
		private Session _nullSession;
		
		//List of Mock Questions
		private List<Question> _questions;
		
		//Used for rendering the component
		private IRenderedComponent<Course> _cut;
		
		[SetUp]
		public void Setup() {
			_ctx = new TestContext(); 
			_ctx.Services.AddTestAuthorization();
			_ctx.Services.AddRazorPages();
			_ctx.Services.AddMockJSRuntime();

			//Create Mock services
			_ctx.Services.AddSingleton<NavigationManager>(new MockNavigationManager());
			
			//Create Mock ViewModels
			_viewModelMock = Mock.Create<TinyCourseViewModel>();
			_ctx.Services.AddSingleton(_viewModelMock);
			
			//Create Mock Sessions
			_mockSession = new Session("My Session", DateTime.Now, DateTime.Now);
			_nullSession = null;
			
			//Create Mock Questions
			_questions = new List<Question> {
				new Question(new IdentityUser(), "Question Title", "Question Content"), 
				new Question(new IdentityUser(), "The second Question", "More Question Content")
			};

			//Arrange the mock methods required to render the component
			Mock.Arrange(
				() => _viewModelMock.SelectedSession
			).Returns(_mockSession);

			Mock.Arrange(
				() => _viewModelMock.Questions
			).Returns(_questions);
			
			//Render the component
			_cut = _ctx.RenderComponent<Course>(("CourseCode", "MYCODE"), ("SelectionIndex", null), ComponentParameterFactory.CascadingValue(_viewModelMock),
				ComponentParameterFactory.CascadingValue(new IdentityUser()), ComponentParameterFactory.CascadingValue(new SessionComponent()));
		}

		[Test]
		public void DisplayCorrectSessionName() {
			//Arrange
			

			//Act
			
			
			//Assert
			_cut.Find("#SessionNameHeader").TextContent.MarkupMatches(_mockSession.Name);
		}
		
		[Test]
		public void DisplayQuestionsWhenNoSession() {
			//Arrange
			Mock.Arrange(
				() => _viewModelMock.SelectedSession
			).Returns(_nullSession);
			
			//Re-Render component so the session's name can change
			_cut.Render();
			
			//Act
			
			
			//Assert
			_cut.Find("#SessionNameHeader").TextContent.MarkupMatches("Questions");
		}
		
		[Test]
		public void DisplaysCorrectNumberOfQuestions() {
			//Arrange
			

			//Act
			
			
			//Assert
			Assert.AreEqual(_questions.Count, _cut.FindComponents<QuestionCard>().Count);
		}
		
		[Test]
		public void DisplayProgressCircleWhenNoSession() {
			//Arrange
			Mock.Arrange(
				() => _viewModelMock.SelectedSession
			).Returns(_nullSession);

			//Re-Render component so the number of Questions can change
			_cut.Render();
			
			//Act
			
			
			//Assert
			Assert.AreEqual(1, _cut.FindComponents<MatProgressCircle>().Count);
		}
	}
}