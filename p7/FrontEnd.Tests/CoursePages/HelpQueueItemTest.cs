﻿using Bunit;
using Bunit.TestDoubles.Authorization;
using Bunit.TestDoubles.JSInterop;
using Common.Data;
using FrontEnd.Pages.CoursePages;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using TestContext = Bunit.TestContext;

namespace FrontEnd.Tests.CoursePages {
	public class HelpQueueItemTest {
		//Test Context
		private TestContext _ctx;

		//Mock PhysicalQuestion
		private PhysicalQuestion _mockQuestion;

		//Used for rendering the component
		private IRenderedComponent<HelpQueueItem> _cut;

		[SetUp]
		public void Setup() {
			_ctx = new TestContext();
			_ctx.Services.AddTestAuthorization();
			_ctx.Services.AddRazorPages();
			_ctx.Services.AddMockJSRuntime();

			//Create Mock PhysicalQuestion
			_mockQuestion = new PhysicalQuestion("Help me", "Where I am at", new IdentityUser(), "I have a question");

			//Render the component
			_cut = _ctx.RenderComponent<HelpQueueItem>(("UserQuestion", _mockQuestion));
		}

		[Test]
		public void QuestionLocationCorrect() {
			//Arrange


			//Act


			//Assert
			_cut.Find("#LocationText").TextContent.MarkupMatches(_mockQuestion.Location!);
		}

		[Test]
		public void PopupContentCorrect() {
			//Arrange


			//Act


			//Assert
			_cut.Find("#QuestionPopup").TextContent.MarkupMatches(_mockQuestion.Title + "\n" + _mockQuestion.Content);
		}
	}
}