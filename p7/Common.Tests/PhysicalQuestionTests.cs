﻿using System;
using Common.Data;
using Common.Protos.PhysicalQuestion;
using Common.Protos.User;
using Common.Utility;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Identity;
using NUnit.Framework;

namespace Common.Tests {
	public class PhysicalQuestionTests {
		

		[Test]
		public void ToPhysicalQuestion_ReturnsPhysicalQuestion() {
			// Aarange
			var physicalQuestionProto = new PhysicalQuestionProto(){Title="testTitle", Location = "testLocation", 
				User = new UserReplySafe(){UserId = "testId", Email = "test@test.dk", Username = "testUsername",
					IsLockedOut = false, IsConfirmed = true}, Content = "testContent", PostDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())};

			// Act
			var result = physicalQuestionProto.ToPhysicalQuestion();
			
			// Assert
			Assert.AreEqual(typeof(PhysicalQuestion), result.GetType());
		}
		
		[Test]
		public void ToPhysicalQuestion_GetsTheInformation() {
			// Arrange
			var title = "testTitle";
			var location = "testLocation";
			var content = "testContent";
			var postDate = DateTime.Now;
			
			var physicalQuestionProto = new PhysicalQuestionProto(){Title=title, Location = location, 
				User = new UserReplySafe(){Username = "testUser"}, Content = content, PostDate = Timestamp.FromDateTime(postDate.ToUniversalTime())};

			// Act
			var result = physicalQuestionProto.ToPhysicalQuestion();
			
			// Assert
			Assert.AreEqual(title, result.Title);
			Assert.AreEqual(location, result.Location);
			Assert.AreEqual(content, result.Content);
			Assert.AreEqual(postDate, result.PostDate);
		}
		
		[Test]
		public void ToPhysicalQuestion_ReturnsNullIfProtoIsNull() {
			// Arrange
			var physicalQuestionProto = new PhysicalQuestionProto();
			
			// Act
			var result = physicalQuestionProto.ToPhysicalQuestion();

			// Assert
			Assert.IsNull(result);
		}
		
		[Test]
		public void ToPhysicalQuestion_ReturnsNullIfUserIsNull() {
			// Arrange
			var physicalQuestionProto = new PhysicalQuestionProto() {
				Title = "testTile",Location = "testLocation",
				Content = "testContent", PostDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), User = null};
			
			// Act
			var result = physicalQuestionProto.ToPhysicalQuestion();

			// Assert 
			Assert.IsNull(result);
		}

		[Test]
		public void ToPhysicalQuestionProto_ReturnsProto() {
			// Arrange
			var physicalQuestion = new PhysicalQuestion("testTitle", "testLocation", new IdentityUser(), 
				"testContent");

			// Act
			var result = physicalQuestion.ToPhysicalQuestionProto();
			
			// Assert
			Assert.AreEqual(typeof(PhysicalQuestionProto), result.GetType());
		}
		
		[Test]
		public void ToPhysicalQuestionProto_GetsTheInformation() {
			// Arrange
			var title = "testTitle";
			var location = "testLocation";
			var content = "testContent";

			var physicalQuestion = new PhysicalQuestion(title,location,new IdentityUser(),content);

			// Act
			var result = physicalQuestion.ToPhysicalQuestionProto();
			
			// Assert
			Assert.AreEqual(title, result.Title);
			Assert.AreEqual(location, result.Location);
			Assert.AreEqual(content, result.Content);
		}
		
		[Test]
		public void ToPhysicalQuestionProto_ReturnsNullIfQuestionIsNull() {
			// Arrange
			var physicalQuestion = new PhysicalQuestion();
			
			// Act
			var result = physicalQuestion.ToPhysicalQuestionProto();

			// Assert
			Assert.IsNull(result);
		}

	}
}