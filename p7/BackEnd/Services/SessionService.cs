﻿using System;
using System.Threading.Tasks;
using BackEnd.Database;
using Common.Data;
using Common.Protos.Session;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Core;

namespace BackEnd.Services {
	public sealed class SessionService : SessionHandler.SessionHandlerBase {
		private readonly DatabaseContext _applicationDbContext;

		public SessionService(DatabaseContext applicationDbContext) {
			_applicationDbContext = applicationDbContext;
		}

		public override async Task<SessionReply> RequestSession(SessionRequestById request, ServerCallContext context) {
			Session? session = await _applicationDbContext.Sessions.FindAsync(request.SessionId);
			if (session is null) {
				return new SessionReply
					{Request = RequestResult.FailRequest($"No session with the id: {request.SessionId}")};
			}

			return new SessionReply {Request = RequestResult.SucceededRequest(), Session = session.ToSessionReply()};
		}

		public override async Task<RequestResult>
			RequestCreateSession(SessionCreationRequest request, ServerCallContext context) {
			Session? session = await _applicationDbContext.Sessions.FindAsync(request.Session.SessionId);
			if (session is not null) {
				return RequestResult.FailRequest($"Already a session with the id: {request.Session.SessionId}");
			}
			
			session = request.Session.ToSession();

			if (session is null) {
				return RequestResult.FailRequest("Could not convert session proto to session model");
			}
			
			var course = await _applicationDbContext.Courses.FindAsync(request.OwningCourseCode);
			if (course is null) {
				return RequestResult.FailRequest("The course with the courseCode does not exist");
			}

			session.Course = course;

			await _applicationDbContext.Sessions.AddAsync(session);
			int result = await _applicationDbContext.SaveChangesAsync();

			return result == 0
				? RequestResult.FailRequest("Failed to add the session to the database.")
				: RequestResult.SucceededRequest();
		}

		public override async Task<RequestResult> RequestDeleteSession(SessionRequestById request,
			ServerCallContext context) {
			Session? session = await _applicationDbContext.Sessions.FindAsync(request.SessionId);
			if (session is null) {
				return RequestResult.FailRequest($"No session with the id: {request.SessionId}");
			}

			_applicationDbContext.Sessions.Remove(session);
			int result = await _applicationDbContext.SaveChangesAsync();

			return result == 0
				? RequestResult.FailRequest("Failed to add the session to the database.")
				: RequestResult.SucceededRequest();
		}

		public override async Task<RequestResult>
			RequestUpdateSession(SessionProto request, ServerCallContext context) {
			Session? session = await _applicationDbContext.Sessions.FindAsync(request.SessionId);
			if (session is null) {
				return RequestResult.FailRequest($"No session with the id: {request.SessionId}");
			}

			session.Name = request.Name;
			session.StartDate = request.StartDate.ToDateTime();
			session.EndDate = request.EndDate.ToDateTime();
			await _applicationDbContext.SaveChangesAsync();

			return RequestResult.SucceededRequest();
		}
	}
}