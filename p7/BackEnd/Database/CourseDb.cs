using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using Common.Data;
using Microsoft.AspNetCore.Identity;

namespace BackEnd.Database {
	public class CourseDb : Course {
		[Required] public List<IdentityUserCustom> ParticipantsDb { get; set; } = new List<IdentityUserCustom>();

		public CourseDb(string name, int year, bool isFall, IdentityUser? user, string subTitle = "") {
			Name = name;
			SubTitle = subTitle;
			Year = year;
			IsFall = isFall;
			Creator = user ?? throw new NoNullAllowedException();
			CourseCode = "";
		}

		public CourseDb(string courseCode, string name, int year, bool isFall, IdentityUser? user,
			string subTitle = "") :
			this(name, year, isFall, user, subTitle) {
			CourseCode = courseCode;
		}

		public CourseDb(string courseCode, string name, int year, bool isFall, IdentityUser? user,
			List<IdentityUserCustom?> participants, List<Session?> sessions, string subTitle = "") :
			this(courseCode, name, year, isFall, user, subTitle) {
			if (sessions.TrueForAll(identityUser => identityUser is null)) throw new NoNullAllowedException();
			Sessions = sessions!;
			if (participants.TrueForAll(identityUser => identityUser is null)) throw new NoNullAllowedException();
			ParticipantsDb = participants!;
		}

		private CourseDb() {
			Name = "";
			SubTitle = "";
			CourseCode = "";
			Creator = null!;
		}
	}
}