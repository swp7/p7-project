using System.Linq;
using Common.Protos.Course;
using Common.Protos.Utility;
using Common.Utility;

namespace BackEnd.Database {
	public static class RequestConversions {
		public static CourseReply ToCourseReply(this CourseDb? course) {
			if (course?.CourseCode is not null)
				return new CourseReply {
					TinyCourse = course.ToTinyCourseReply(),
					Sessions = {course.Sessions.Select(session => session.ToSessionReply())},
					Participants = {course.ParticipantsDb.Select(user => user.ToUserReplySafe())}
				};

			var result = new CourseReply {TinyCourse = new CourseTinyReply()};
			//TODO: Might not be the best place to do this
			result.TinyCourse.Result = RequestResult.FailRequest("Course was not found!");
			return result;
		}
	}
}