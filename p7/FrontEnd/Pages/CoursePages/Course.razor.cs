﻿using System;
using System.Collections.Generic;
using Common.Data;
using FrontEnd.Pages.CoursePages.Components;
using FrontEnd.ViewModels;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Pages.CoursePages {
	public partial class Course : IDisposable {
		[Parameter] public string CourseCode { get; set; }

		[Parameter] public int? SelectionIndex { get; set; }

		[CascadingParameter] public IdentityUser User { get; set; }

		public string GetSelectedSessionName() {
			if (ViewModel?.SelectedSession is not null) {
				return ViewModel.SelectedSession.Name;
			}

			return "Questions";
		}

		[CascadingParameter] public TinyCourseViewModel ViewModel { get; set; }

		[CascadingParameter] public SessionComponent SessionComponent { get; set; }


		protected override void OnInitialized() {
			CourseCode = CourseCode.ToUpper();
			base.OnInitialized();
			SessionComponent.OnUpdatedSession += ViewModel.UpdateActiveQuestions;

			if (SelectionIndex != null)
				SessionComponent.SetSelectedSession(SelectionIndex.Value);

			ViewModel.PropertyChanged += async (_, _) => { await InvokeAsync(StateHasChanged); };
		}


		private string GetCourseLink() => $"Course/{ViewModel.Course.CourseCode}";

		public void Dispose() {
			SessionComponent.OnUpdatedSession -= ViewModel.UpdateActiveQuestions;
		}
	}
}