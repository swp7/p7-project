using System;
using System.Net.Http;
using FrontEnd.Areas.Identity;
using FrontEnd.PubSub;
using FrontEnd.Services;
using FrontEnd.Storage;
using FrontEnd.ViewModels;
using Grpc.Core;
using Grpc.Net.Client;
using JetBrains.Annotations;
using MatBlazor;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

namespace FrontEnd {
	public class Startup {
		//The ENV variable from the Dockerfile, to change the GRPC address of the BackEnd.
		[CanBeNull] private static readonly string BackendHostEnv =
			Environment.GetEnvironmentVariable("BACKEND_CONNECTION_STRING");

		public static bool IsContainerized => Environment.GetEnvironmentVariable("IS_CONTAINERIZED") != null;

		//The standard GRPC channel to use. If the ENV var is not set, assume you are running backend locally
		public static GrpcChannel CreateStandardNewChannel =>
			CreateNewChannel(BackendHostEnv ?? "http://localhost:6969");

		//Make the default channel. Insecure because we can not use HTTPS because the GRPC service is not public facing
		public static GrpcChannel CreateNewChannel(string host) {
			AppContext.SetSwitch(
				"System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
			var httpHandler = new HttpClientHandler {
				ServerCertificateCustomValidationCallback =
					HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
			};
			return GrpcChannel.ForAddress(host,
				new GrpcChannelOptions {Credentials = ChannelCredentials.Insecure, HttpHandler = httpHandler});
		}

		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services) {
			services.AddRazorPages();
			services.AddServerSideBlazor();
			services.AddHealthChecks();
			services.AddHttpClient();
			services.AddViewModels();
			services.AddServices();
			services.RegisterSubs();
            
			services.AddMatToaster(config =>
			{
				config.Position = MatToastPosition.TopCenter;
				config.PreventDuplicates = true;
				config.NewestOnTop = true;
				config.ShowCloseButton = true;
				config.MaximumOpacity = 95;
				config.VisibleStateDuration = 4000;
			});

			services.AddScoped(typeof(IUserStore<>).MakeGenericType(typeof(IdentityUser)), typeof(UserStore));
			services.AddScoped(typeof(IRoleStore<>).MakeGenericType(typeof(IdentityRole)), typeof(RoleStore));

			services.AddDefaultIdentity<IdentityUser>(options => {
				options.Stores.MaxLengthForKeys = 128;
				options.SignIn.RequireConfirmedAccount = false;
				options.SignIn.RequireConfirmedEmail = false;
				options.Password.RequireDigit = false;
				options.Password.RequireUppercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequiredLength = 4;
			}).AddRoles<IdentityRole>();

			//This replaces the default SignInManager with our custom one which logs in with email instead of username.
			var descriptor =
				new ServiceDescriptor(
					typeof(SignInManager<IdentityUser>),
					typeof(CustomSignInManager), ServiceLifetime.Scoped);
			services.Replace(descriptor);

			services
				.AddScoped<AuthenticationStateProvider, RevalidatingIdentityAuthenticationStateProvider<IdentityUser>
				>();
		}


		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
			app.UseWebSockets();
			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
			}
			else {
				app.UseExceptionHandler("/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseAuthentication();

			//TODO: Https maybe?
			//app.UseHttpsRedirection();
			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints => {
				endpoints.MapControllers();
				endpoints.MapBlazorHub();
				endpoints.MapFallbackToPage("/_Host");
			});
		}
	}

	internal static class Extenstion {
		internal static void AddViewModels(this IServiceCollection services) {
			services.AddScoped<IdentityUserViewModel>();
			services.AddScoped<TinyCourseViewModel>();
		}

		internal static void AddServices(this IServiceCollection services) {
			services.AddScoped<ICourseService, CourseService>();
			services.AddScoped<IRoleService, RoleService>();
			services.AddScoped<IQuestionService, QuestionService>();
			services.AddScoped<ISessionService, SessionService>();
		}

		internal static void RegisterSubs(this IServiceCollection services) {
			if (!Startup.IsContainerized)
				Console.WriteLine(
					"WARNING: Not running Containerized! Pub/Sub won't work.\nTo set up correctly see: https://gitlab.com/swp7/k8sconfigs");
			services.AddSingleton<PhysicalQuestionSubscription>();
			services.AddSingleton<QuestionSubscription>();
			services.AddSingleton<QuestionAnswerSubscription>();
			services.AddSingleton<CommentSubscription>();
		}
	}
}
