using System;
using System.Collections.Generic;
using Common.Data;
using Common.PubSub.Data;
using FrontEnd.PubSub;
using FrontEnd.Services;

namespace FrontEnd.ViewModels {
	public class PhysicalQuestionViewModel : BaseViewModel, IDisposable {
		private List<PhysicalQuestion> _questions = new List<PhysicalQuestion>();
		private bool _isInitialized;
		private readonly string _courseId;
		private readonly PhysicalQuestionSubscription _physicalQuestionSubscription;

		public virtual List<PhysicalQuestion> Questions {
			get {
				if (_isInitialized) return _questions;
				Refresh();
				_isInitialized = true;

				return _questions;
			}
			private set => SetValue(ref _questions, value);
		}

		public PhysicalQuestionViewModel(string courseId, PhysicalQuestionSubscription physicalQuestionSubscription,
			IQuestionService qService) {
			_courseId = courseId;
			_physicalQuestionSubscription = physicalQuestionSubscription;
			physicalQuestionSubscription.PostableEvent.Subscribe(OnPhysicalQuestionUpdate);
		}

		public void OnPhysicalQuestionUpdate(PostableUpdate postableUpdate) {
			if (!postableUpdate.CourseCode.Equals(_courseId, StringComparison.OrdinalIgnoreCase)) return;
			switch (postableUpdate.UpdateType) {
				case UpdateType.Created:
					//TODO: use the question id to get the new one.
					Refresh();
					break;
				case UpdateType.Deleted:
				case UpdateType.Updated:
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(UpdateType), postableUpdate.UpdateType, null);
			}

			OnPropertyChanged(nameof(Questions));
			IsBusy = false;
		}

		public void Refresh() {
			IsBusy = true;
			using (IQuestionService qService = new QuestionService()) {
				Questions = qService.GetAllPhysicalQuestionsForCourse(_courseId);
			}

			IsBusy = false;
		}

		public void Dispose() {
			_physicalQuestionSubscription.PostableEvent.UnSubscribe(OnPhysicalQuestionUpdate);
		}
	}
}