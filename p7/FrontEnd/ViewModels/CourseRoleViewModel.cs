﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Data;
using FrontEnd.Services;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.ViewModels {
	public class CourseRoleViewModel : BaseViewModel {
		private TinyCourse _course = new Course();
		private Dictionary<IdentityUser, string> _userRoleMap = new();

		public TinyCourse Course {
			get => _course;
			private set => SetValue(ref _course, value);
		}

		public virtual Dictionary<IdentityUser, string> UserRoleMap {
			get => _userRoleMap;
			private set => SetValue(ref _userRoleMap, value);
		}

		public CourseRoleViewModel(string courseCode, ICourseService courseService) {
			Course = courseService.RequestTinyCourse(courseCode);
		}

		public string GetRole(IdentityUser user) {
			return _userRoleMap[user];
		}

		public virtual async Task UpdateCourseUsers(ICourseService courseService) {
			IsBusy = true;
			_userRoleMap =
				await courseService.GetCourseParticipantsAsync(_course.CourseCode);
			IsBusy = false;
		}
	}
}