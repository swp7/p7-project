﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Storage {
	public partial class UserStore {
		public override Task AddLoginAsync(IdentityUser user, UserLoginInfo login,
			CancellationToken cancellationToken = new()) {
			throw new NotImplementedException();
		}

		public override Task RemoveLoginAsync(IdentityUser user, string loginProvider, string providerKey,
			CancellationToken cancellationToken = new()) {
			throw new NotImplementedException();
		}

		public override Task<IList<UserLoginInfo>> GetLoginsAsync(IdentityUser user,
			CancellationToken cancellationToken = new()) {
			return Task.FromResult((IList<UserLoginInfo>) new List<UserLoginInfo>());
			throw new NotImplementedException();
		}

		protected override Task<IdentityUserLogin<string>> FindUserLoginAsync(string userId, string loginProvider,
			string providerKey, CancellationToken cancellationToken) {
			throw new NotImplementedException();
		}

		protected override Task<IdentityUserLogin<string>> FindUserLoginAsync(string loginProvider, string providerKey,
			CancellationToken cancellationToken) {
			throw new NotImplementedException();
		}
	}
}