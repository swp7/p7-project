﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Common.Protos.Roles;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Storage {
	public class RoleStore : RoleStoreBase<IdentityRole, string, IdentityUserRole<string>, IdentityRoleClaim<string>> {
		private readonly RoleHandler.RoleHandlerClient _roleClient;


		public RoleStore(IdentityErrorDescriber describer) : base(describer) {
			GrpcChannel channel = Startup.CreateStandardNewChannel;
			_roleClient = new RoleHandler.RoleHandlerClient(channel);
		}

		public override async Task<IdentityResult> CreateAsync(IdentityRole role,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RequestResult result = await _roleClient.RequestCreateRoleAsync(new RoleReply() {
				Id = role.Id,
				Name = role.Name,
				ConcurrencyStamp = role.ConcurrencyStamp,
				NormalizedName = role.NormalizedName
			}, cancellationToken: cancellationToken);
			return result.ToIdentityResult();
		}

		public override async Task<IdentityResult> UpdateAsync(IdentityRole role,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RequestResult result = await _roleClient.RequestUpdateRoleAsync(new RoleReply() {
				Id = role.Id,
				Name = role.Name,
				ConcurrencyStamp = role.ConcurrencyStamp,
				NormalizedName = role.NormalizedName
			}, cancellationToken: cancellationToken);
			return result.ToIdentityResult();
		}

		public override async Task<IdentityResult> DeleteAsync(IdentityRole role,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RequestResult result = await _roleClient.RequestDeleteRoleAsync(new RoleRequestById() {
				RoleId = role.Id
			}, cancellationToken: cancellationToken);
			return result.ToIdentityResult();
		}

		public override async Task<IdentityRole> FindByIdAsync(string id,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RoleReply result = await _roleClient.RequestRoleByIdAsync(new RoleRequestById() {
				RoleId = id
			}, cancellationToken: cancellationToken);

			return new IdentityRole {
				Id = result.Id, Name = result.Name, ConcurrencyStamp = result.ConcurrencyStamp,
				NormalizedName = result.NormalizedName
			};
		}

		public override async Task<IdentityRole> FindByNameAsync(string normalizedName,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RoleReply result = await _roleClient.RequestRoleByNameAsync(new RoleRequestByName() {
				NormalizedName = normalizedName
			}, cancellationToken: cancellationToken);
			if (string.IsNullOrEmpty(result.Id))
				return null;
			return new IdentityRole {
				Id = result.Id, Name = result.Name, ConcurrencyStamp = result.ConcurrencyStamp,
				NormalizedName = result.NormalizedName
			};
		}

		public override Task<IList<Claim>> GetClaimsAsync(IdentityRole role,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			return Task.FromResult<IList<Claim>>(new List<Claim>());
			//TODO: not sure we need this since we aren't using claims.
		}

		public override Task AddClaimAsync(IdentityRole role, Claim claim,
			CancellationToken cancellationToken = new()) {
			throw new NotImplementedException();
		}

		public override Task RemoveClaimAsync(IdentityRole role, Claim claim,
			CancellationToken cancellationToken = new()) {
			throw new NotImplementedException();
		}

		public override IQueryable<IdentityRole> Roles {
			get {
				return _roleClient.RequestAllRoles(new Empty()).Roles.Select(reply => new IdentityRole() {
					Id = reply.Id,
					Name = reply.Name,
					ConcurrencyStamp = reply.ConcurrencyStamp,
					NormalizedName = reply.NormalizedName
				}).AsQueryable();
			}
		}
	}
}