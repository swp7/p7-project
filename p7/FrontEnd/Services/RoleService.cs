﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Protos.Roles;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Services {
	public sealed class RoleService : IDisposable, IRoleService {
		private readonly RoleHandler.RoleHandlerClient _client;
		private readonly GrpcChannel _channel;

		public RoleService() {
			_channel = Startup.CreateStandardNewChannel;
			_client = new RoleHandler.RoleHandlerClient(Startup.CreateStandardNewChannel);
		}

		public RoleService(string host) {
			_client = new RoleHandler.RoleHandlerClient(Startup.CreateNewChannel(host));
		}

		public List<string> RequestRolesForUser(IdentityUser user) {
			return _client.RequestGetRoles(new UserRequestById {UserId = user.Id}).Roles
				.Select(request => request.Name).ToList();
		}

		public IList<IdentityUser> RequestUsersInRoleAsync(IdentityRole role) {
			UserListReply users =
				_client.RequestUsersInRole(new RoleRequestByName {NormalizedName = role.Name});
			if (users.Users is null || users.Users.Count == 0)
				return new List<IdentityUser>();

			return users.Users.Select(safe => safe.ToIdentityUser()).ToList();
		}

		public bool RequestIsUserInRole(IdentityUser user, string role) {
			UserIsInRoleReply result = _client.RequestIsInRole(new UserRoleRequestByName()
				{NormalizedRoleName = role, UserId = user.Id});
			return RequestConversions.CheckForSuccess(result.RequestResult) && result.IsInRole;
		}

		public void RequestAddToRoleAsync(IdentityUser user, string normalizedRoleName) {
			RequestResult result = _client.RequestAddToRole(
				new UserRoleRequestByName {UserId = user.Id, NormalizedRoleName = normalizedRoleName});
			RequestConversions.CheckForSuccess(result);
		}

		public void RequestRemoveFromRoleAsync(IdentityUser user, string normalizedRoleName) {
			RequestResult result = _client.RequestRemoveFromRole(
				new UserRoleRequestByName {UserId = user.Id, NormalizedRoleName = normalizedRoleName});
			RequestConversions.CheckForSuccess(result);
		}

		public void Dispose() {
			_channel?.Dispose();
		}
	}
}