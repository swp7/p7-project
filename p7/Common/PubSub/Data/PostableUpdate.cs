﻿namespace Common.PubSub.Data {
	public class PostableUpdate {
		
		public string CourseCode { get; init; }
		public UpdateType UpdateType { get; init; }
		public int PostableId { get; init; }

		public PostableUpdate(string courseCode, UpdateType updateType, int postableId) {
			CourseCode = courseCode;
			UpdateType = updateType;
			PostableId = postableId;
		}
	}

	public enum UpdateType {
		Created,
		Deleted,
		Updated
	}
}