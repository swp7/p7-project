using System;
using System.Net.Http;
using Common.Protos.Token;
using Microsoft.AspNetCore.Identity;

namespace Common.Utility {
	public static partial class RequestConversions {
		public static IdentityUserToken<string>? ToIdentityUserToken(this TokenReply? tokenReply) {
			if (tokenReply?.TokenValue is null || tokenReply.TokenName is null)
				return null;

			return new IdentityUserToken<string>{LoginProvider = tokenReply.TokenLoginProvider, 
				Name = tokenReply.TokenName, UserId = tokenReply.TokenUserId, Value = tokenReply.TokenValue
			};
		}

		public static TokenReply? ToTokenReply(this IdentityUserToken<string>? identityUserToken) {
			if (identityUserToken?.Value is null || identityUserToken.Name is null)
				return null;

			return new TokenReply{TokenLoginProvider = identityUserToken.LoginProvider, 
				TokenName = identityUserToken.Name, TokenUserId = identityUserToken.UserId,
				TokenValue = identityUserToken.Value
			};
		}
	}
}