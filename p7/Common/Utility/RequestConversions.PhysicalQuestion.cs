﻿using System;
using Common.Protos.PhysicalQuestion;
using Common.Data;
using Google.Protobuf.WellKnownTypes;

namespace Common.Utility {
	public static partial class RequestConversions {
		public static PhysicalQuestion? ToPhysicalQuestion(
			this PhysicalQuestionProto? physicalQuestionProto) {
			if (physicalQuestionProto?.Title is null) return null;

			var user = physicalQuestionProto.User.ToIdentityUser();


			if (user is null)
				return null;


			return new PhysicalQuestion(physicalQuestionProto.Title,
					physicalQuestionProto.Location, user,
					physicalQuestionProto.Content)
				{PostDate = physicalQuestionProto.PostDate.ToDateTime().ToLocalTime()};
		}

		public static PhysicalQuestionProto? ToPhysicalQuestionProto(
			this PhysicalQuestion? physicalQuestion) {
			if (physicalQuestion?.Title is not null && !String.IsNullOrWhiteSpace(physicalQuestion.Location)
			                                        && physicalQuestion.PostDate != default
			                                        && physicalQuestion.Poster is not null) {
				return new PhysicalQuestionProto() {
					Title = physicalQuestion.Title,
					Location = physicalQuestion.Location, User = physicalQuestion.Poster.ToUserReplySafe(),
					Content = physicalQuestion.Content,
					PostDate = Timestamp.FromDateTime(physicalQuestion.PostDate.ToUniversalTime())
				};
			}

			return null;
		}
	}
}