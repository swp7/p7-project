using System;
using System.Linq;
using System.Security.Claims;
using Common.Protos.Claims;

namespace Common.Utility {
	public static partial class RequestConversions {
		public static ProtoClaim? ToProtoClaim(this Claim? claim) {
			if (claim?.Type == null) return null;
			return new ProtoClaim() {
				Issuer = claim.Issuer,
				Subject = claim.Subject?.ToIdentityClaimProto(),
				Type = claim.Type,
				Value = claim.Value,
				ValueType = claim.ValueType,
				OriginalIssuer = claim.OriginalIssuer,
				PropertiesKeys = {claim.Properties.Keys},
				PropertiesValues = {claim.Properties.Values}
			};
		}

		public static IdentityClaimProto? ToIdentityClaimProto(this ClaimsIdentity? claim) {
			if (claim?.Label == null) return null;

			return new IdentityClaimProto() {
				Actor = claim.Actor.ToIdentityClaimProto(),
				Label = claim.Label,
				AuthenticationType = claim.AuthenticationType,
				Claims = {claim.Claims.Select(ToProtoClaim)},
				NameClaimType = claim.NameClaimType,
				RoleClaimType = claim.RoleClaimType
			};
		}

		public static Claim? ToClaim(this ProtoClaim? claim) {
			if (claim?.Type == null) return null;
			var result = new Claim(claim.Type, claim.Value, claim.ValueType, claim.Issuer, claim.OriginalIssuer,
				claim.Subject.ToClaimsIdentity());
			var index = 0;
			foreach (string propertiesKey in claim.PropertiesKeys) {
				result.Properties.Add(propertiesKey, claim.PropertiesValues[index]);
				index++;
			}

			return result;
		}

		public static ClaimsIdentity? ToClaimsIdentity(this IdentityClaimProto? claim) {
			if (claim?.Label == null) return null;

			var result = new ClaimsIdentity(claim.AuthenticationType, claim.NameClaimType, claim.RoleClaimType) {
				Actor = claim.Actor.ToClaimsIdentity(),
				Label = claim.Label,
			};
			foreach (ProtoClaim protoClaim in claim.Claims) {
				var newClaim = protoClaim.ToClaim();
				if (newClaim != null)
					result.AddClaim(newClaim);
				else
					Console.WriteLine("Failed to create claim.");
			}

			return result;
		}
	}
}