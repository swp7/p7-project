﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Data;
using Common.Protos.Course;
using Common.Protos.PhysicalQuestion;
using Common.Protos.Utility;
using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;

namespace Common.Utility {
	public static partial class RequestConversions {
		public static Question? ToQuestion(this QuestionReplyNoAnswers? questionReply) {
			if (questionReply?.Postable.Id == null) return null;

			var res = new Question(questionReply.Postable.Poster.ToIdentityUser(), questionReply.Title,
				questionReply.Postable.Content, null) {
				IsSolved = questionReply.IsSolved,
				IsUnderstood = questionReply.IsUnderstood,
				IsBeingLookedAtByTA = questionReply.IsBeingLookedAtByTA,
				PostDate = questionReply.Postable.PostDate.ToDateTime().ToLocalTime(),
				Id = questionReply.Postable.Id
			};
			foreach (TagReply tagReply in questionReply.Postable.Tags) {
				var tag = tagReply.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					res.Tags.Add(tag);
				}
			}

			return res;
		}

		public static Question? ToQuestion(this QuestionsReplyWithAnswers? questionsReplyWithAnswers) {
			var res = questionsReplyWithAnswers?.Question.ToQuestion();
			if (res == null) {
				return null;
			}

			if (questionsReplyWithAnswers?.Answers is not null && questionsReplyWithAnswers.Answers.Count != 0) {
				res.QuestionAnswers = questionsReplyWithAnswers.Answers.Select(ToQuestionAnswer).ToList();
			}

			return res;
		}


		public static Question? ToQuestion(this CreateQuestionRequest? questionReply) {
			if (questionReply?.Title == null) return null;

			var res = new Question(questionReply.Poster.ToIdentityUser(), questionReply.Title,
				questionReply.Content, null) {
				PostDate = questionReply.PostDate.ToDateTime().ToLocalTime()
			};
			foreach (TagReply tagReply in questionReply.Tags) {
				var tag = tagReply.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					res.Tags.Add(tag);
				}
			}

			return res;
		}
		
		public static QuestionAnswer? ToQuestionAnswer(this CreateQuestionAnswerCommentRequest? questionReply) {
			if (questionReply?.Poster == null) return null;

			var res = new QuestionAnswer(questionReply.Poster.ToIdentityUser(), questionReply.Content, null) {
				PostDate = questionReply.PostDate.ToDateTime().ToLocalTime(),
				Question = new Question() {Id = questionReply.BelongsTo}
			};
			foreach (TagReply tagReply in questionReply.Tags) {
				var tag = tagReply.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					res.Tags.Add(tag);
				}
			}

			return res;
		}
		
		public static Comment? ToComment(this CreateQuestionAnswerCommentRequest? questionReply) {
			if (questionReply?.Poster == null) return null;

			var res = new Comment(questionReply.Poster.ToIdentityUser(),
				questionReply.Content) {
				PostDate = questionReply.PostDate.ToDateTime().ToLocalTime(),
				QuestionAnswer = new QuestionAnswer(null, null) {Id = questionReply.BelongsTo}
			};
			
			foreach (TagReply tagReply in questionReply.Tags) {
				var tag = tagReply.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					res.Tags.Add(tag);
				}
			}

			return res;
		}

		public static CreateQuestionRequest? ToCreateRequest(this Question? question) {
			if (question?.Title == null) return null;

			var res = new CreateQuestionRequest {
				Poster = question.Poster.ToUserReplySafe(), Content = question.Content,
				Title = question.Title, PostDate = question.PostDate.ToUniversalTime().ToTimestamp(),
				SessionId = question.OwningSession.Id
			};
			foreach (Tag tag in question.Tags) {
				TagReply? tagReply = tag.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					res.Tags.Add(tagReply);
				}
			}

			return res;
		}
		
		public static CreateQuestionAnswerCommentRequest? ToCreateQuestionAnswerCommentRequest(
			this QuestionAnswer? questionAnswer) {
			if (questionAnswer?.Poster == null || questionAnswer.Question is null) return null;

			var res = new CreateQuestionAnswerCommentRequest {
				Poster = questionAnswer.Poster.ToUserReplySafe(), Content = questionAnswer.Content, 
				PostDate = questionAnswer.PostDate.ToUniversalTime().ToTimestamp(), 
				BelongsTo = questionAnswer.Question.Id
			};
			foreach (Tag tag in questionAnswer.Tags) {
				TagReply? tagReply = tag.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					res.Tags.Add(tagReply);
				}
			}

			return res;
		}
		
		public static CreateQuestionAnswerCommentRequest? ToCreateQuestionAnswerCommentRequest(this Comment? comment) {
			if (comment?.Poster == null || comment.QuestionAnswer?.Id == null) return null;

			var res = new CreateQuestionAnswerCommentRequest {
				Poster = comment.Poster.ToUserReplySafe(), Content = comment.Content, 
				PostDate = comment.PostDate.ToUniversalTime().ToTimestamp(),
				BelongsTo = comment.QuestionAnswer.Id
			};
			foreach (Tag tag in comment.Tags) {
				TagReply? tagReply = tag.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					res.Tags.Add(tagReply);
				}
			}

			return res;
		}

		public static QuestionAnswer ToQuestionAnswer(this QuestionAnswerReply answerReply) {
			List<Tag> tagList = new();

			if (answerReply.Postable.Tags is not null) {
				foreach (TagReply tagReply in answerReply.Postable.Tags) {
					var tag = tagReply.ToTag();
					if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
						tagList.Add(tag);
					}
				}
			}

			var res = new QuestionAnswer(answerReply.Postable.Poster.ToIdentityUser(), answerReply.Postable.Content,
				null, tagList) {
				IsMarkedCorrect = answerReply.IsMarkedCorrect,
				Id = answerReply.Postable.Id,
				PostDate = answerReply.Postable.PostDate.ToDateTime().ToLocalTime()
			};
			if (answerReply.Comments != null && answerReply.Comments.Count != 0)
				res.Comments = answerReply.Comments.Select(ToComment).ToList();
			return res;
		}

		public static Comment? ToComment(this CommentReply commentReply) {
			if (commentReply.Comment.Poster == null) return null;
			
			var res = new Comment(commentReply.Comment.Poster.ToIdentityUser(), commentReply.Comment.Content) {
				Id = commentReply.Comment.Id,
				PostDate = commentReply.Comment.PostDate.ToDateTime().ToLocalTime(),
				OwningSession = null!
			};
			
			foreach (TagReply tagReply in commentReply.Comment.Tags) {
				var tag = tagReply.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					res.Tags.Add(tag);
				}
			}

			return res;
		}

		public static PostableReply? ToPostableReply(this Postable postable) {
			if (postable?.Poster is null) {
				return null;
			}

			return new PostableReply {
				Content = postable.Content,
				Id = postable.Id,
				Poster = postable.Poster.ToUserReplySafe(),
				PostDate = Timestamp.FromDateTime(postable.PostDate.ToUniversalTime()),
				Tags = {postable.Tags.Select(tag => tag.ToTag())}
			};
		}

		public static CommentReply? ToCommentReply(this Comment? comment) {
			if (comment?.Poster is null) {
				return null;
			}

			return new CommentReply {
				Comment = comment.ToPostableReply()
			};
		}

		public static QuestionAnswerReply? ToAnswerReply(this QuestionAnswer? answer) {
			if (answer?.Poster is null) {
				return null;
			}

			return new QuestionAnswerReply {
				Postable = answer.ToPostableReply(),
				IsMarkedCorrect = answer.IsMarkedCorrect,
				Comments = {answer.Comments.Select(ToCommentReply)}
			};
		}

		public static QuestionReplyNoAnswers? ToQuestionReplyNoAnswers(this Question? question) {
			if (question?.Poster is null) {
				return null;
			}

			return new QuestionReplyNoAnswers {
				Postable = question.ToPostableReply(),
				Title = question.Title,
				IsSolved = question.IsSolved,
				IsUnderstood = question.IsUnderstood,
				IsBeingLookedAtByTA = question.IsBeingLookedAtByTA
			};
		}

		public static QuestionsReplyWithAnswers? ToQuestionReplyWithAnswers(this Question? question) {
			if (question?.Poster is null) {
				return null;
			}

			if (question.QuestionAnswers.Count == 0) {
				return new QuestionsReplyWithAnswers {
					Question = question.ToQuestionReplyNoAnswers(),
					Result = RequestResult.SucceededRequest()
				};
			}

			return new QuestionsReplyWithAnswers {
				Question = question.ToQuestionReplyNoAnswers(),
				Answers = {question.QuestionAnswers.Select(ToAnswerReply)},
				Result = RequestResult.SucceededRequest()
			};
		}
	}
}