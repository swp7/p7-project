﻿using Common.Protos.Utility;

// ReSharper disable once CheckNamespace
namespace Common.Protos.Utility {
	public partial class RequestResult {
		public static RequestResult FailRequest() {
			return new() {Succeeded = false};
		}

		public static RequestResult FailRequest(params string[] errors) {
			return new() {Succeeded = false, Errors = {errors}};
		}

		public static RequestResult SucceededRequest() {
			return new() {Succeeded = true};
		}
	}
}