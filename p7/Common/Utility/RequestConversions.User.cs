﻿using Common.Protos.User;
using Microsoft.AspNetCore.Identity;

namespace Common.Utility {
	public static partial class RequestConversions {
		public static IdentityUser? ToIdentityUser(this UserReplySafe? userReplySafe) {
			if (userReplySafe?.Username is null || userReplySafe.Email is null) {
				return null;
			}

			return new IdentityUser {
				Id = userReplySafe.UserId,
				UserName = userReplySafe.Username,
				NormalizedUserName = userReplySafe.Username.ToUpper(),
				Email = userReplySafe.Email,
				NormalizedEmail = userReplySafe.Email.ToUpper()
			};
		}

		public static IdentityUser? ToIdentityUser(this UserReply? userReply) {
			if (userReply?.User?.Username is null || userReply.User.Email is null) {
				return null;
			}

			return new IdentityUser {
				Id = userReply.User.UserId,
				UserName = userReply.User.Username,
				NormalizedUserName = userReply.User.Username.ToUpper(),
				Email = userReply.User.Email,
				NormalizedEmail = userReply.User.Email.ToUpper(),
				PasswordHash = userReply.PasswordHash
			};
		}

		public static UserReplySafe? ToUserReplySafe(this IdentityUser? user) {
			if (user?.UserName is null || user.Email is null)
				return null;
			return new UserReplySafe {
				Email = user.Email, Username = user.UserName,
				IsConfirmed = user.EmailConfirmed, UserId = user.Id,
				IsLockedOut = user.LockoutEnabled
			};
		}

		public static UserReply? ToUserReply(this IdentityUser? user) {
			if (user?.UserName is null || user.Email is null || user.PasswordHash is null)
				return null;
			return new UserReply {
				User = user.ToUserReplySafe(),
				PasswordHash = user.PasswordHash
			};
		}
	}
}