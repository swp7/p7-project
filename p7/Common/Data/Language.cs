using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;

namespace Common.Data {
	public class Language {
		[Key] [Required] public string LanguageName { get; set; }

		public Language(string language) {
			LanguageName = language;
		}

		[UsedImplicitly]
		private Language() {
			LanguageName = "";
		}
	}
}