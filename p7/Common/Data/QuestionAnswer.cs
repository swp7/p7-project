using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;

namespace Common.Data {
	public class QuestionAnswer : Postable {
		public bool IsMarkedCorrect { get; set; }
		public List<Comment> Comments { get; set; } = new();
		public Question? Question { get; set; }

		public QuestionAnswer(IdentityUser poster, string content, Session? session = null,
			List<Tag>? tags = null) : base(poster, content, session) {
			if (tags != null)
				Tags = tags;
		}

		[UsedImplicitly]
		private QuestionAnswer() : base(null!, "", null) { }
	}
}