using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;

namespace Common.Data {
	public class Comment : Postable {
		public QuestionAnswer QuestionAnswer { get; set; }

		public Comment(IdentityUser poster, string content,
			Session session = null!) : base(poster, content, session) { }

		[UsedImplicitly]
		private Comment() : base(null!, "", null!) { }
	}
}