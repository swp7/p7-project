using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;

namespace Common.Data {
	public class MultiMediaItem {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Required]
		public int ItemId { get; set; }

		[Required] public string PathToItem { get; set; }
		[Required] public Postable Owner { get; set; }

		public MultiMediaItem(int itemId, string pathToItem, Postable owner) {
			ItemId = itemId;
			PathToItem = pathToItem;
			Owner = owner;
		}

		[UsedImplicitly]
		private MultiMediaItem() {
			PathToItem = "";
			Owner = null!;
		}
	}
}