using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using JetBrains.Annotations;

namespace Common.Data {
	public class Session {
		[Key]
		[Required]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		[Required] public string Name { get; set; }
		[Required] public DateTime StartDate { get; set; }

		[Required] public DateTime EndDate { get; set; }
		[Required] public List<Question> Questions { get; set; } = new List<Question>();
		[Required] public virtual List<PhysicalQuestion> PhysicalQuestions { get; set; } = new List<PhysicalQuestion>();

		[Required] public virtual Course Course { get; set; }

		public bool IsActive() => DateTime.Now >= StartDate && DateTime.Now < EndDate;


		public Session(string name, DateTime startDate, DateTime endDate) {
			Name = name;
			StartDate = startDate;
			EndDate = endDate;
		}


		[UsedImplicitly]
		private Session() {
			Name = "";
		}
	}
}