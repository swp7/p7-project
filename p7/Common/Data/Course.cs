using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;

namespace Common.Data {
	public class Course : TinyCourse {
		[Required] public List<Session> Sessions { get; set; } = new List<Session>();
		[Required] public List<IdentityUser> Participants { get; set; } = new List<IdentityUser>();

		public Course(string name, int year, bool isFall, IdentityUser? user, string subTitle = "") {
			Name = name;
			SubTitle = subTitle;
			Year = year;
			IsFall = isFall;
			Creator = user ?? throw new NoNullAllowedException();
			CourseCode = "";
		}

		public Course(string courseCode, string name, int year, bool isFall, IdentityUser? user,
			string subTitle = "") :
			this(name, year, isFall, user, subTitle) {
			CourseCode = courseCode;
		}

		public Course(string courseCode, string name, int year, bool isFall, IdentityUser? user,
			List<IdentityUser?> participants, List<Session?> sessions, string subTitle = "") :
			this(courseCode, name, year, isFall, user, subTitle) {
			if (sessions.TrueForAll(identityUser => identityUser is null)) throw new NoNullAllowedException();
			Sessions = sessions!;
			if (participants.TrueForAll(identityUser => identityUser is null)) throw new NoNullAllowedException();
			Participants = participants!;
		}

		public Course() {
			Name = "";
			SubTitle = "";
			CourseCode = "";
			Creator = null!;
		}
	}
}