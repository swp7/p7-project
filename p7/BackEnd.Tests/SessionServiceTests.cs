﻿using System;
using System.Linq;
using BackEnd.Services;
using Common.Protos.Session;
using Common.Protos.Utility;
using Google.Protobuf.WellKnownTypes;
using NUnit.Framework;

namespace BackEnd.Tests {
	public class SessionServiceTests {
		private DataForTest _dataForTest;
		private SessionService _sessionService;

		[SetUp]
		public void SetUp() {
			_dataForTest?.Dispose();
			_dataForTest = new DataForTest();
			_sessionService = new SessionService(_dataForTest.Db);
		}

		[Test]
		public void RequestSession_Succeed() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = _dataForTest.Session1.Id};
			
			// Act
			SessionReply result = _sessionService.RequestSession(sessionRequestById, null).Result;

			// Assert
			Assert.True(result.Request.Succeeded);
		}
		
		[Test]
		public void RequestSession_GetsCorrectSession() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = _dataForTest.Session1.Id};
			
			// Act
			SessionReply result = _sessionService.RequestSession(sessionRequestById, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.Session1.Id, result.Session.SessionId);
			Assert.AreEqual(_dataForTest.Session1.Name, result.Session.Name);
			Assert.AreEqual(Timestamp.FromDateTime(_dataForTest.Session1.StartDate.ToUniversalTime()), result.Session.StartDate);
			Assert.AreEqual(Timestamp.FromDateTime(_dataForTest.Session1.EndDate.ToUniversalTime()), result.Session.EndDate);
		}
		
		[Test]
		public void RequestSession_SessionMustExist() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = 0};
			
			// Act
			SessionReply result = _sessionService.RequestSession(sessionRequestById, null).Result;

			// Assert
			Assert.False(result.Request.Succeeded);
		}

		[Test]
		public void RequestCreateSession_Succeed() {
			// Arrange
			var sessionCreationRequest = new SessionCreationRequest(){OwningCourseCode = _dataForTest.Course1.CourseCode, 
				Session = new SessionProto(){SessionId = 0, Name = "SessionTestName", StartDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), 
					EndDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())}};
			
			// Act
			RequestResult result = _sessionService.RequestCreateSession(sessionCreationRequest, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}
		
		[Test]
		public void RequestCreateSession_SessionIdMustBeUnique() {
			// Arrange
			var sessionCreationRequest = new SessionCreationRequest(){OwningCourseCode = _dataForTest.Course1.CourseCode, 
				Session = new SessionProto(){SessionId = 1, Name = "SessionTestName", StartDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), 
					EndDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())}};
			
			// Act
			RequestResult result = _sessionService.RequestCreateSession(sessionCreationRequest, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}
		
		[Test]
		public void RequestCreateSession_CourseMustExist() {
			// Arrange
			var sessionCreationRequest = new SessionCreationRequest(){OwningCourseCode = "0", 
				Session = new SessionProto(){SessionId = 0, Name = "SessionTestName", StartDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), 
					EndDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())}};
			
			// Act
			RequestResult result = _sessionService.RequestCreateSession(sessionCreationRequest, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void RequestDeleteSession_Succeed() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = _dataForTest.Session1.Id};
			
			// Act
			RequestResult result = _sessionService.RequestDeleteSession(sessionRequestById, null).Result;
			
			// Assert
			Assert.True(result.Succeeded);
		}
		
		[Test]
		public void RequestDeleteSession_RemovesSessions() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = _dataForTest.Session1.Id};
			int amountOfSession = _dataForTest.Db.Sessions.ToList().Count;
			
			// Act
			RequestResult result = _sessionService.RequestDeleteSession(sessionRequestById, null).Result;
			int amountOfSessionAfter = _dataForTest.Db.Sessions.ToList().Count;
			
			// Assert
			Assert.AreEqual(amountOfSession-1, amountOfSessionAfter);
		}
		
		[Test]
		public void RequestDeleteSession_SessionMustExist() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = 0};

			// Act
			RequestResult result = _sessionService.RequestDeleteSession(sessionRequestById, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void RequestUpdateSession_Succeed() {
			// Arrange
			var sessionProto = new SessionProto(){SessionId = 1, Name = "newName", 
				StartDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), EndDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())};
			
			// Act
			RequestResult result = _sessionService.RequestUpdateSession(sessionProto, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}
		
		[Test]
		public void RequestUpdateSession_UpdatesSession() {
			// Arrange
			var newName = "newName";
			var sessionProto = new SessionProto(){SessionId = _dataForTest.Session1.Id, Name = newName, 
				StartDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), EndDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())};

			// Act
			RequestResult result = _sessionService.RequestUpdateSession(sessionProto, null).Result;

			// Assert
			Assert.AreEqual(newName, _dataForTest.Db.Sessions.ToList().Find(s => s.Id == _dataForTest.Session1.Id).Name);
		}
		
		[Test]
		public void RequestUpdateSession_SessionMustExist() {
			// Arrange
			var sessionProto = new SessionProto(){SessionId = 0, Name = "newName", 
				StartDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), EndDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())};

			// Act
			RequestResult result = _sessionService.RequestUpdateSession(sessionProto, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}
		
		
	}
}