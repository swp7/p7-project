#cd p7
docker build --pull -t "registry.gitlab.com/swp7/p7-project/frontend:rasmus" -f DockerfileFrontEnd . &
docker build --pull -t "registry.gitlab.com/swp7/p7-project/backend:rasmus" -f DockerfileBackEnd . &
wait
docker push "registry.gitlab.com/swp7/p7-project/backend:rasmus"&
docker push "registry.gitlab.com/swp7/p7-project/frontend:rasmus"&
wait
